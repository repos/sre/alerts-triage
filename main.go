package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/prometheus/alertmanager/cli"
	"golang.org/x/exp/slog"
)

var (
	configFile = flag.String("config", "/etc/alerts-triage.yml", "config file path")
	listenAddr = flag.String("addr", ":3000", "listen address")
	debugLog   = flag.Bool("debug", false, "set debug log level")
)

func replaceLogAttrs(groups []string, a slog.Attr) slog.Attr {
	// Don't log time when running as a systemd service
	if a.Key == slog.TimeKey && os.Getenv("INVOCATION_ID") != "" {
		return slog.Attr{}
	}
	return a
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	var programLevel = new(slog.LevelVar) // Info by default
	h := slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: programLevel, ReplaceAttr: replaceLogAttrs})
	slog.SetDefault(slog.New(h))
	if *debugLog {
		programLevel.Set(slog.LevelDebug)
	}

	config, err := NewConfig(*configFile)
	if err != nil {
		log.Fatal(err)
	}

	router := chi.NewRouter()
	router.Use(middleware.Logger)
	router.Use(middleware.Timeout(2 * time.Second))

	if config.URLPrefix != "" {
		// Make sure URLPrefix requests always have a trailing slash.
		// This is to ensure js/css loading works as expected, without the need
		// to specify URLPrefix in templates.
		router.Use(func(next http.Handler) http.Handler {
			fn := func(w http.ResponseWriter, r *http.Request) {
				if r.Method == "GET" && strings.EqualFold(r.URL.Path, config.URLPrefix) {
					newURL := &url.URL{}
					*newURL = *r.URL
					newURL.Path = newURL.Path + "/"
					http.Redirect(w, r, newURL.String(), 301)
				}
				next.ServeHTTP(w, r)
			}
			return http.HandlerFunc(fn)
		})

		router.Use(func(next http.Handler) http.Handler {
			return http.StripPrefix(config.URLPrefix, next)
		})
	}

	am := cli.NewAlertmanagerClient(config.AlertmanagerURL.URL)

	phabricator, err := NewPhabricator(config)
	if err != nil {
		log.Fatal(err)
	}

	triager, err := NewTriager(config, phabricator, am)
	if err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	triager.Start(ctx, router)

	slog.Info("Listening", "addr", *listenAddr, "prefix", config.URLPrefix)
	err = http.ListenAndServe(*listenAddr, router)
	if err != nil {
		log.Fatal(err)
	}
}
