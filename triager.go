package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/go-chi/chi/v5"
	"github.com/go-openapi/strfmt"
	"github.com/prometheus/alertmanager/api/v2/client"
	"github.com/prometheus/alertmanager/api/v2/client/alertgroup"
	"github.com/prometheus/alertmanager/api/v2/models"
	"golang.org/x/exp/slog"
	"gopkg.in/yaml.v3"
)

var severityOrder = map[string]int{
	"critical": 0,
	"warning":  1,
	"info":     2,
	"unknown":  3,
}

// Triager contains the logic to serve alerts in a "triageable" form.
type Triager struct {
	config     *Config
	templates  *template.Template
	am         *client.AlertmanagerAPI
	alerts     []*models.AlertGroup
	alertsLock sync.RWMutex
}

// Start sets up the Triager and its processes.
func (t *Triager) Start(ctx context.Context, r chi.Router) error {
	if err := t.installRoutes(r); err != nil {
		return err
	}

	// A stub alerts file has been provided, pretend that is the Alertmanager response
	if t.config.StubAlertsPath != "" {
		f, err := os.ReadFile(t.config.StubAlertsPath)
		if err != nil {
			return err
		}
		err = yaml.Unmarshal(f, &t.alerts)
		if err != nil {
			return err
		}

		return nil
	}

	// Otherwise, do a first update and start refreshing in the background
	t.UpdateAlerts(ctx)
	go t.RefreshAlerts(ctx)

	return nil
}

func (t *Triager) installRoutes(r chi.Router) error {
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		err := t.templates.ExecuteTemplate(w, "index.html", t.getAlerts())
		if err != nil {
			http.Error(w, err.Error(), 503)
		}
	})

	// Convenience method to get raw alerts
	r.Get("/alerts", func(w http.ResponseWriter, r *http.Request) {
		enc := yaml.NewEncoder(w)
		enc.Encode(t.getAlerts())
	})

	staticHandler := http.FileServer(http.FS(staticFS))
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		staticHandler.ServeHTTP(w, r)
	})

	return nil
}

// NewTriager creates a new Triager. The Triager is in charge of fetching alerts and serve pages.
func NewTriager(config *Config, phabricator *Phabricator, am *client.AlertmanagerAPI) (*Triager, error) {
	templateFuncs := template.FuncMap{
		"humanizeDate": func(date strfmt.DateTime) string {
			return humanize.Time(time.Time(date))
		},
		"numAlerts": func(groups []*models.AlertGroup) int {
			res := 0
			for _, group := range groups {
				res += len(group.Alerts)
			}
			return res
		},
		"ellipsis": func(str string, length int) string {
			if len(str) <= length {
				return str
			}
			return str[:length-3] + "..."
		},
		"newTaskUrl": func(alert models.GettableAlert) string {
			url, err := phabricator.NewTaskURL(alert)
			if err != nil {
				return err.Error()
			}
			return url.String()
		},
		"tooltipData": func(alert models.GettableAlert, excludes ...string) string {
			res := make([]string, 0)
			for key, value := range alert.Labels {
				skip := false
				for _, ex := range excludes {
					if strings.EqualFold(ex, key) {
						skip = true
					}
				}
				if skip {
					continue
				}
				res = append(res, fmt.Sprintf("%s=%s", key, value))
			}
			return strings.Join(res, " ")
		},
	}
	templates := template.New("html").Funcs(templateFuncs)

	templates, err := templates.ParseFS(templatesFS, "templates/*.html")
	if err != nil {
		return nil, err
	}

	t := Triager{templates: templates, config: config, am: am}

	return &t, nil
}

// UpdateAlerts fetches alerts from Alertmanager.
func (t *Triager) UpdateAlerts(ctx context.Context) error {
	toutCtx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	params := alertgroup.NewGetAlertGroupsParamsWithContext(toutCtx)
	resp, err := t.am.Alertgroup.GetAlertGroups(params)
	if err != nil {
		return err
	}

	t.alertsLock.Lock()
	defer t.alertsLock.Unlock()

	t.alerts = make([]*models.AlertGroup, len(resp.Payload))
	for i, group := range resp.Payload {
		v := *group
		t.alerts[i] = &v
	}
	slog.DebugContext(ctx, "Refreshed alerts from Alertmanager", "numAlerts", len(t.alerts))

	return nil
}

// RefreshAlerts makes sure alerts are updated periodically.
func (t *Triager) RefreshAlerts(ctx context.Context) {
	ticker := time.NewTicker(time.Duration(t.config.RefreshAlertsSeconds) * time.Second)
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			err := t.UpdateAlerts(ctx)
			if err != nil {
				slog.ErrorContext(ctx, "Failed to refresh alerts", "err", err.Error())
			}
		}
	}
}

func (t *Triager) getAlerts() []*models.AlertGroup {
	t.alertsLock.RLock()
	defer t.alertsLock.RUnlock()
	return t.processAlerts(t.alerts)
}

// processAlerts contains the logic of what alerts should be displayed and in what order.
func (t *Triager) processAlerts(alerts []*models.AlertGroup) []*models.AlertGroup {
	res := make([]*models.AlertGroup, 0)
	alertsSeen := make(map[string]bool)
	groupsSeen := make(map[string]bool)

	now := time.Now()

	for _, group := range alerts {
		// groups can be repeated with only varying "receiver"
		// therefore de-duplicate based on group labels
		fp, _ := yaml.Marshal(group.Labels)
		if _, seen := groupsSeen[string(fp)]; seen {
			continue
		}
		groupsSeen[string(fp)] = true

		filteredAlerts := make([]*models.GettableAlert, 0)

		for _, alert := range group.Alerts {
			// alert fingerprint-based de-duplication
			if _, seen := alertsSeen[*alert.Fingerprint]; seen {
				continue
			}
			alertsSeen[*alert.Fingerprint] = true

			startTime := time.Time(*alert.StartsAt)
			if now.Before(startTime.Add(t.config.HideAlertsOlderThan.Duration)) {
				continue
			}

			if *alert.Status.State != "active" {
				continue
			}

			severity, found := alert.Labels["severity"]
			if !found || strings.ToLower(severity) == "task" {
				continue
			}

			filteredAlerts = append(filteredAlerts, alert)
		}

		if len(filteredAlerts) > 0 {
			ag := models.AlertGroup{}
			ag.Labels = group.Labels
			ag.Receiver = group.Receiver
			ag.Alerts = filteredAlerts
			res = append(res, &ag)
		}
	}

	// Sort alerts oldest to newest, within their respective groups
	for _, group := range res {
		sort.Slice(group.Alerts, func(i, j int) bool {
			return startsBefore(group.Alerts[i], group.Alerts[j])
		})
	}

	// Sort groups by most severe to least severe, and oldest to newest.
	sort.Slice(res, func(i, j int) bool {
		// Only the first alert in each group needs to be considered, because:
		// * the first alert is already the oldest, due to sorting above
		// * we group by severity already so a group can't contain different severities
		iAlert := res[i].Alerts[0]
		jAlert := res[j].Alerts[0]

		iSev, iFound := severityOrder[iAlert.Labels["severity"]]
		jSev, jFound := severityOrder[jAlert.Labels["severity"]]

		// Alerts with unknown severity (or no severity) sort last
		if !iFound || !jFound {
			return false
		}

		// Sort by severity first
		if iSev != jSev {
			return iSev < jSev
		}

		// Then sort by start time
		return startsBefore(iAlert, jAlert)
	})

	return res
}

func startsBefore(i, j *models.GettableAlert) bool {
	return time.Time(*i.StartsAt).Before(time.Time(*j.StartsAt))
}
