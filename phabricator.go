package main

import (
	"bytes"
	"fmt"
	"html/template"
	"net/url"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/go-openapi/strfmt"
	"github.com/prometheus/alertmanager/api/v2/models"
	"github.com/prometheus/common/model"
)

// PhabricatorRoute represents instructions on how tasks should be routed.
type PhabricatorRoute struct {
	Matchers Matchers `yaml:"matchers"`
	Continue bool     `yaml:"continue"`
	Priority string   `yaml:"priority"`
	Projects []string `yaml:"projects"`
}

// Matches returns true if any route matcher matches the LabelSet.
func (r *PhabricatorRoute) Matches(lset model.LabelSet) bool {
	for _, m := range r.Matchers {
		labelValue := lset[model.LabelName(m.Name)]
		if !m.Matches(string(labelValue)) {
			return false
		}
	}
	return true
}

// Phabricator handles the logic of building new task URLs from routes and alerts.
type Phabricator struct {
	config    *Config
	templates *template.Template
}

// NewPhabricator creates a new Phabricator from Config.
func NewPhabricator(config *Config) (*Phabricator, error) {
	templateFuncs := template.FuncMap{
		"humanizeDate": func(date strfmt.DateTime) string {
			return humanize.Time(time.Time(date))
		},
		"alertDashboardURL": func(alert models.GettableAlert) template.HTML {
			dash := &url.URL{}
			*dash = *config.AlertDashboardURL.URL
			values := url.Values{
				"q": {
					"alertname=" + alert.Labels["alertname"],
					"@state=active",
				},
			}
			dash.RawQuery = values.Encode()
			// Disable auto-escape to avoid translating "+" in query string into HTML entities
			return template.HTML(dash.String())
		},
		"alertGeneratorURL": func(alert models.GettableAlert) template.HTML {
			return template.HTML(alert.GeneratorURL.String())
		},
	}

	templates := template.New("tmpl").Funcs(templateFuncs)
	templates, err := templates.ParseFS(templatesFS, "templates/phabricator/*.tmpl")
	if err != nil {
		return nil, err
	}

	return &Phabricator{config: config, templates: templates}, nil
}

// routesForAlert returns all routes that match lset.
func (p *Phabricator) routesForAlert(alert models.GettableAlert) []*PhabricatorRoute {
	lset := buildLabelSet(alert.Labels)
	res := make([]*PhabricatorRoute, 0)

	for i, route := range p.config.PhabricatorRoutes {
		if route.Matches(lset) {
			res = append(res, &p.config.PhabricatorRoutes[i])
			if !route.Continue {
				break
			}
		}
	}

	return res
}

// NewTaskURL returns the URL to create a new task for the given alert.
func (p *Phabricator) NewTaskURL(alert models.GettableAlert) (*url.URL, error) {
	routes := p.routesForAlert(alert)
	values, err := p.urlValuesForRoutes(routes)
	if err != nil {
		return nil, err
	}

	alertValues, err := p.urlValuesForAlert(alert)
	if err != nil {
		return nil, err
	}

	// Extend routes values with alert values
	for key, value := range *alertValues {
		values.Set(key, value[0])
	}

	subs := p.config.PhabricatorSubscribers
	if len(subs) > 0 {
		values.Set("subscribers", strings.Join(subs, ","))
	}

	// Build the final url
	res := &url.URL{}
	*res = *p.config.PhabricatorURL.URL
	res.Path = fmt.Sprintf("/maniphest/task/edit/form/%d/", p.config.ManiphestID)
	res.RawQuery = values.Encode()

	return res, nil
}

// urlValuesForRoutes returns URL values (query string) for the given routes.
func (p *Phabricator) urlValuesForRoutes(routes []*PhabricatorRoute) (*url.Values, error) {
	res := &url.Values{}
	projects := make([]string, 0)

	for _, route := range routes {
		res.Set("priority", route.Priority)
		projects = append(projects, route.Projects...)
	}

	if len(projects) > 0 {
		res.Set("projects", strings.Join(projects, ","))
	}

	return res, nil
}

// urlValuesForAlert returns URL values (query string) for the given alert.
func (p *Phabricator) urlValuesForAlert(alert models.GettableAlert) (*url.Values, error) {
	var titleBuf, descBuf bytes.Buffer
	res := &url.Values{}

	err := p.templates.ExecuteTemplate(&titleBuf, "title.tmpl", alert)
	if err != nil {
		return nil, err
	}
	res.Set("title", titleBuf.String())

	err = p.templates.ExecuteTemplate(&descBuf, "description.tmpl", alert)
	if err != nil {
		return nil, err
	}
	res.Set("description", descBuf.String())

	return res, nil
}

// Adapter between AM's API types (models.LabelSet) and Prometheus
// (model.LabelSet) to be able to match LabelSets
func buildLabelSet(labelsMap models.LabelSet) model.LabelSet {
	labelSet := model.LabelSet{}
	for key, value := range labelsMap {
		labelSet[model.LabelName(key)] = model.LabelValue(value)
	}
	return labelSet
}
