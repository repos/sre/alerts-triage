build:
	go mod download
	go build

test:
	go test

install:
	install -D -m 755 alerts-triage $(DESTDIR)/usr/bin/alerts-triage

clean:
	go clean
